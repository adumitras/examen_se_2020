package marire;

import java.util.List;

public class S1 {
	static class A { }

	static class B extends A {			// inherited from A
		private String param;
		D d;
		C c;
		public B(D d) {
			this.d = d;			// association with D
			this.c = new C();	// composition with C
		}
	}
	static class Z {
		B b;					// associated with B
		public Z(B b) { this.b = b; }
		public void g() {}
	}
	class E {
		B b;
		public E(B b) { this.b = b; }
	}
	static class C { }
	static class D {
		public void f() {}
	}
}
