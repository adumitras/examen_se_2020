package marire;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import javax.swing.*;

public class S2 extends JFrame {        //GUI
	JPanel jp = new JPanel();
	JTextField jt = new JTextField(20);
	JTextArea jta = new JTextArea(30, 30);
	JButton jb = new JButton("Let's see");

	public S2() {
		setTitle("Marire");
		setVisible(true);
		setSize(600, 700);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		jp.add(jt);
		jp.add(jta);
		jta.setEditable(false);
		jta.setLocation(10,10);
		jp.add(jb);
		jb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					File f = new File(jt.getText());
					BufferedReader br = new BufferedReader(new FileReader(f));
					String line;
					while ((line = br.readLine()) != null) {
						jta.append(line + "\n");
					}
				} catch (FileNotFoundException nfe) {
					jta.setText("No such file or directory.");
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		});
		jb.setLocation(2,2);
		add(jp);
	}

	public static void main(String[] args) {
		S2 gui = new S2();
	}
}
