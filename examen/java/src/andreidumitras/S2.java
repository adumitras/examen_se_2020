package andreidumitras;

public class S2 {
	public static class Th1 extends Thread {
		public void run() {
			for (int i = 0; i < 9; i++) {
				System.out.println("[SE-Thread1] - [" + (i + 1) + "]");
				try {
					Thread.sleep(6000);		// sleep 6 seconds
				} catch (Exception ex) {}
			}
		}
	}
	public static class Th2 extends Thread {
		public void run() {
			for (int i = 0; i < 9; i++) {
				System.out.println("[SE-Thread2] - [" + (i + 1) + "]");
				try {
					Thread.sleep(6000);		// sleep 6 seconds
				} catch (Exception ex) {}
			}
		}
	}
	public static class Th3 extends Thread {
		public void run() {
			for (int i = 0; i < 9; i++) {
				System.out.println("[SE-Thread3] - [" + (i + 1) + "]");
				try {
					Thread.sleep(6000);		// sleep 6 seconds
				} catch (Exception ex) {}
			}
		}
	}

	public static void main(String[] args) {
		Th1 t1 = new Th1();
		Th2 t2 = new Th2();
		Th3 t3 = new Th3();

		t1.start();
		t2.start();
		t3.start();
		System.out.println("Je finis.");
	}

}
