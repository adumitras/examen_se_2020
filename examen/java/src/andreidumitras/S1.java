package andreidumitras;

import java.io.Serializable;

public class S1 {

    static class A {
        private M mm;
        public A(M m) { this.mm = m; }      // aggregation
    }

    static class B {
        private M mm;
        public B() { this.mm = new M(); }   // composition
    }

    static class M {
        public void mNu() {
            System.out.println("MMMda");
        }
        public void mDa() {
            System.out.println("MMMMnu");
        }
    }

    static class L {
        private int a;
        private M mm;

        public void b() { }
        public void i() { }
        public void hmm(int eh) {       // association
            if (eh > 3) {
                mm.mDa();
            } else {
                mm.mNu();
            }
        }

    }

    static class X {
        public void met(L l) { }       // another association
    }

    static class I implements Serializable {
        public void i() { }
    }
}
